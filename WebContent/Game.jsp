<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Cream</title>
</head>
<body>
	<%@ page import="au.edu.cis.unimelb.swen9007.group32.cream.domain.*" %>
	<%@ page import="org.apache.shiro.subject.Subject" %>
	<%@ page import="org.apache.shiro.SecurityUtils" %>
	<%@ page import="au.edu.cis.unimelb.swen9007.group32.cream.session.AppSession" %>
	<% 
		ServiceLayer sl = new ServiceLayer();
		Game g = sl.findGame(Integer.parseInt(request.getParameter("id")));
		Subject currentUser = SecurityUtils.getSubject();
	%>
	<% if (g != null) { %>
	<h3><% out.print(g.getTitle()); %></h3>
	<p>
		<i>$<% out.print(g.getPrice()); %></i><br><br>
		Qty left: <% out.print(g.getQty()); %><br>
		Developer: <% out.print(g.getDeveloper().getName()); %><br>
		Media Type: <% out.print(g.getMedia().getId()); %><br>
		Description: <% out.print(g.getDescription()); %><br><br>
		<% if (currentUser.getPrincipal() == null) {
			out.print("<i>Log in to make purchase</i>");
		} else if (currentUser.hasRole(AppSession.CUSTOMER_ROLE)) {
			%>
			
		<form action="./Order">
			<input type="hidden" name="id" value=<% out.print(g.getId()); %>>
			<button type="submit">Make Order</button>
		</form>	
				
		<%
		}
		%>
		<br><br>
		<a href="./">Home</a>
	</p>
	<% } else { %>
	<p>
		No Game Found
	</p>
		<a href="./">Home</a>|<a href="./login">Log In</a>
	<% } %>

</body>
</html>