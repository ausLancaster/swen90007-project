<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Purchase Successful</title>
</head>
<body>
	<p>Thanks <%=request.getAttribute("clientName")%><p>
	<p>You have successfully purchased <%=request.getAttribute("gameName")%></p>
	<a href="./">Home</a> | <a href="./purchases">Purchase History</a>
</body>
</html>