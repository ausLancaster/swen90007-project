<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Cream</title>
</head>
<body>
	<%@ page import="au.edu.cis.unimelb.swen9007.group32.cream.domain.*" %>
	<% 
		ServiceLayer sl = new ServiceLayer();
		Game g = sl.findGame(Integer.parseInt(request.getParameter("id")));
	%>
	<% if (g != null) { %>
	<h3>Order details:</h3>
	<p>
		<%=request.getAttribute("quantFail") %>
		<%=request.getAttribute("lockFail") %>
		<form action="./Buy">
			Qty:<input type="number" name="qty" min="1" max="<% out.print(g.getQty()); %>"><br>
		    x <i>$<% out.print(g.getPrice()); %></i><br><br>
		    <input type="hidden" name="id" value=<% out.print(g.getId()); %>>
			<button type="submit">Buy</button>
		</form>
		<br><br>
		<a href="./">Home</a>
	</p>
	<% } else { %>
	<p>Game Not Found</p>
	<a href="./">Home</a>
	<% } %>
	

</body>
</html>