<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Cream</title>
</head>
<body>
	<%@ page import="au.edu.cis.unimelb.swen9007.group32.cream.domain.*" %>
	<%@ page import="org.apache.shiro.subject.Subject" %>
	<%@ page import="org.apache.shiro.SecurityUtils" %>
	<%@ page import="au.edu.cis.unimelb.swen9007.group32.cream.session.AppSession" %>
	<% 
	Subject currentUser = SecurityUtils.getSubject();
	if (currentUser.getPrincipal() == null) {
		out.print("Must be logged in to make purchases");
	} else if (currentUser.hasRole(AppSession.CUSTOMER_ROLE)) {
		ServiceLayer sl = new ServiceLayer();
		Game g = sl.findGame(Integer.parseInt(request.getParameter("id")));
		int qty = Integer.parseInt(request.getParameter("qty"));
		Payment payment = ServiceLayer.makePayment(qty*g.getPrice(), "credit card");
		Customer c = sl.getCustomerFromEmail(currentUser.getPrincipal().toString());
		sl.buyGame(c, qty, payment, g);
		
	%>
	<h3>Game bought!</h3>
	<p>
		<% out.print(g.getTitle()); %><br>
		$<% out.print(payment.getAmount()); %><br><br>
		<% } else {
			out.print("<i>Must be a customer to buy games</i>");
			}%>
		<a href="./">Home</a> | <a href="./purchases">Purchase History</a> | <a href="./logout">Log Out</a>
	</p>

</body>
</html>