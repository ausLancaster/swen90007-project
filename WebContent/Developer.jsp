<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Developer Console</title>
</head>
<%@ page import="au.edu.cis.unimelb.swen9007.group32.cream.domain.*" %>
<%@ page import="java.util.List" %>
<%@ page import="org.apache.shiro.subject.Subject" %>
<%@ page import="org.apache.shiro.SecurityUtils" %>
<%@ page import="au.edu.cis.unimelb.swen9007.group32.cream.session.AppSession" %>
<body>
	<h3>Your games</h3>
	<form action="./UpdateQty">
	<ol>
	<jsp:useBean id="helper" type="au.edu.cis.unimelb.swen9007.group32.cream.domain.DeveloperHelper" scope="request"/>
	<% for (Game g : helper.getAllDevelopedGames()) { %>
		<li><%=g.getTitle() %><br> Qty: <%=g.getQty()%> increase by
		<input type="number" name="<%=g.getId() %>" min="0"></li>
		<br>
		</li>
	<% } %>
	</ol>
	<button type="submit">Increase Stock</button>
	</form>
	<br>
	<a href="./">Home</a>|<a href="./logout">Log Out</a>

</body>
</html>