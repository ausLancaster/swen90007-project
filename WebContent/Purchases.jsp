<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Cream</title>
</head>
<body>
	<h3>Purchase History:</h3>
	<%@ page import="au.edu.cis.unimelb.swen9007.group32.cream.domain.*" %>
	<%@ page import="java.util.List" %>
	<%@ page import="org.apache.shiro.subject.Subject" %>
	<%@ page import="org.apache.shiro.SecurityUtils" %>
	<ol>
		<% 
		Subject currentUser = SecurityUtils.getSubject();
		if (currentUser.getPrincipal() == null) {
			out.print("Must be logged in to view purchase history");
		} else {
			ServiceLayer sl = new ServiceLayer();
			Customer c = sl.findCustomer(((User)currentUser.getPrincipal()).getId());
			List<Purchase> purchases = ServiceLayer.getAllCustomerPurchases(c);
			for (Purchase p : purchases) {
				%>
				<li>
					Item: <% out.print(p.getGame().getTitle()); %><br>
					Qty: <% out.print(p.getQty()); %><br>
					Total: <% out.print(p.getPayment().getAmount()); %>
				</li>
			<% }
		}%>
	</ol>
	<br>
	<a href="./">Home</a>
	
</body>
</html>