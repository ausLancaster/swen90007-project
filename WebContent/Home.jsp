<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Cream</title>
</head>
<body>
	<p> 
	<%@ page import="au.edu.cis.unimelb.swen9007.group32.cream.domain.*" %>
	<%@ page import="au.edu.cis.unimelb.swen9007.group32.cream.session.AppSession" %>
	<%@ page import="java.util.List" %>
	<%@ page import="org.apache.shiro.subject.Subject" %>
	<%@ page import="org.apache.shiro.SecurityUtils" %>
	<%
		Subject currentUser = SecurityUtils.getSubject();
		if (currentUser.getPrincipal() != null) {
			out.print("Welcome, " + ((User)currentUser.getPrincipal()).getName());
		}
		if (currentUser.hasRole(AppSession.DEVELOPER_ROLE)) {
			%> <a href="./Developer">Developer Console</a> <%
		}
	%> 
	</p>
	<h3>Games for sale:</h3>
	<ol><i>
		<% 
		ServiceLayer sl = new ServiceLayer();
		List<Game> games;
		String query = request.getParameter("query");
		if (query != null) {
			games = sl.searchGames(request.getParameter("query"));
			out.print("searched for " + query + ":");
		} else {
			games = sl.listAllGames();			
		}
		%> </i><br><br> <%
		for (Game g : games) {
			%>
			<li>
				<% out.print(g.getTitle()); %>
				<a href="./Game?id=<% out.print(g.getId()); %>">view</a>
			</li>
		<% } %>
	</ol>
	<form action="./search">
	    <input type="text" name="query">
		<button type="submit">Search games</button>
	</form>
	<br><br>
	<% if (currentUser.hasRole(AppSession.CUSTOMER_ROLE)) { %>
	<a href="./purchases">Purchase History</a>|
	<% } %>
	<% if (currentUser.getPrincipal() == null) { %>
		<a href="./login">Log In</a>
	<% } else {%>
		<a href="./logout">Log Out</a>
	<% } %>
</body>
</html>