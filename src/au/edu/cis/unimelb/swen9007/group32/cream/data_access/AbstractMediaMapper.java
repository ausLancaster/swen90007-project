package au.edu.cis.unimelb.swen9007.group32.cream.data_access;

import au.edu.cis.unimelb.swen9007.group32.cream.domain.Media;

public abstract class AbstractMediaMapper {
	public abstract void save(Media media);
	public abstract Media load(int id);
}