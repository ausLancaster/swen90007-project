package au.edu.cis.unimelb.swen9007.group32.cream.distribution;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.InputStream;
import java.io.OutputStream;

public class PaymentDTO {
	private float amount;
	private String method;
	
	public PaymentDTO() {}
	
	public PaymentDTO(float amount, String method) {
		this.amount = amount;
		this.method = method;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}
	
	public static void toXML(PaymentDTO payDTO, OutputStream outputStream) {
		XMLEncoder encoder = new XMLEncoder(outputStream);
		encoder.writeObject(payDTO);
		encoder.close();
	}
	
	public static PaymentDTO fromXML(InputStream inputStream) {
		XMLDecoder decoder = new XMLDecoder(inputStream);
		PaymentDTO payDTO = (PaymentDTO) decoder.readObject();
		decoder.close();
		return payDTO;
	}

}
