package au.edu.cis.unimelb.swen9007.group32.cream.controller;

import java.io.IOException;

import javax.servlet.ServletException;

public class LoginCommand extends FrontCommand {
	@Override
	public void process() throws ServletException, IOException {
		forward("/Login.jsp");
	}
}
