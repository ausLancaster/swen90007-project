package au.edu.cis.unimelb.swen9007.group32.cream.domain;

public class PhysicalMedia extends Media {
	public float packagingSize;
	public String format;
	public PhysicalMedia(int id, float packagingSize, String format) {
		super(id);
		this.packagingSize = packagingSize;
		this.format = format;
	}
	
	public float getPackagingSize() {
		return this.packagingSize;
	}
	
	public String getFormat() {
		return this.format;
	}

	@Override
	public String getMediaType() {
		return "Physical";
	}
}
