package au.edu.cis.unimelb.swen9007.group32.cream.data_access;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import au.edu.cis.unimelb.swen9007.group32.cream.domain.Downloadable;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.Media;

public class DownloadableMediaMapper extends AbstractMediaMapper{
	private final static String insertDownloadableMedia = "INSERT INTO APP.downloadablemedia VALUES (?, ?)";
	private final static String searchDownloadableMedia = "SELECT * FROM APP.downloadablemedia WHERE id = ?";

	public DownloadableMediaMapper() {
		super();
	}
	
	@Override
	public void save(Media media) {
		// TODO Auto-generated method stub
		Downloadable dl = (Downloadable) media;
		int id = dl.getId();
		String url = dl.getDownloadURL();
		PreparedStatement stm = DBConnection.prepare(insertDownloadableMedia);
		try {
			stm.setInt(1, id);
			stm.setString(2, url);
			stm.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	@Override
	public Downloadable load(int id) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		PreparedStatement stm = DBConnection.prepare(searchDownloadableMedia);
		try {
			stm.setInt(1, id);
			ResultSet rs = stm.executeQuery();
			while(rs.next()) {
				int dl_id = rs.getInt("id");
				String url = rs.getString("URL");
				return new Downloadable(dl_id, url);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return null;
	}
}
