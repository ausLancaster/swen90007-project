package au.edu.cis.unimelb.swen9007.group32.cream.data_access;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import au.edu.cis.unimelb.swen9007.group32.cream.domain.Customer;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.Game;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.Payment;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.Purchase;

public class PurchaseMapper {
	private final static String purchaseQry = "SELECT * FROM APP.purchases WHERE id = ?";
	private final static String createPurchase = "INSERT INTO APP.purchases(id, gameID, quantity, paymentMethod, paymentAmount) VALUES (?, ?, ?, ?, ?)";
	private final static String createCustomerPurchase = "INSERT INTO APP.customerpurchase(customerID, purchaseID) VALUES (?, ?)";

	/** Returns the purchase however it will now contain the identifier */
	public void create(Purchase purchase, Customer customer) {
		// TODO Write Purchase to database
		writePurchase(purchase);

		// TODO Write the Customer Purchase
		writeCustomerPurchase(purchase.getId(), customer.getId());
	}

	private void writeCustomerPurchase(int purchaseId, int customerId) {
		// Writes in the Associative Table
		PreparedStatement sqlPrepared = DBConnection.prepare(createCustomerPurchase);
		try {
			sqlPrepared.setInt(1, customerId);
			sqlPrepared.setInt(2, purchaseId);
			sqlPrepared.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void writePurchase(Purchase purchase) {
		// Get payment values to embed into table
		Payment payment = purchase.getPayment();
		String paymentMethod = payment.getMethod();
		float amount = payment.getAmount();

		PreparedStatement sqlPrepared = DBConnection.prepare(createPurchase);
		try {

			sqlPrepared.setInt(1, purchase.getId());
			sqlPrepared.setInt(2, purchase.getGame().getId());
			sqlPrepared.setInt(3, purchase.getQty());
			sqlPrepared.setString(4, paymentMethod);
			sqlPrepared.setFloat(5, amount);
			sqlPrepared.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Purchase getPurchase(int purchaseId) {
		PreparedStatement sqlPurchase = DBConnection.prepare(purchaseQry);
		try {
			sqlPurchase.setInt(1, purchaseId);
			ResultSet rs = sqlPurchase.executeQuery();
			if (rs.next()) {
				GameMapper gameMap = new GameMapper();
				int id = rs.getInt(1);
				int gameId = rs.getInt(2);
				int quantity = rs.getInt(3);
				String paymentMethod = rs.getString(4);
				float paymentAmount = rs.getFloat(5);

				Game game = gameMap.findGame(gameId);
				Payment payment = new Payment(paymentAmount, paymentMethod);

				Purchase purchase = new Purchase(id, payment, game, quantity);
				return purchase;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
