package au.edu.cis.unimelb.swen9007.group32.cream.locking;

public class ConcurrencyException extends Exception {
	public ConcurrencyException() {
		super("ERROR: Lock not avaliable");
	}
}
