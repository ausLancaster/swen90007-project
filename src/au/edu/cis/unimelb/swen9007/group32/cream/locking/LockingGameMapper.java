package au.edu.cis.unimelb.swen9007.group32.cream.locking;

import au.edu.cis.unimelb.swen9007.group32.cream.data_access.GameMapper;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.Game;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.User;

public class LockingGameMapper extends GameMapper {
	private ExclusiveWriteManager elm;
	private User user;
	
	public LockingGameMapper(User user) {
		this.elm = ExclusiveWriteManager.getInstance();
		this.user = user;
	}
	
	public Game getGame(int id) throws NoLockException {
		if (elm.acquireLock(super.findGame(id), user)) {
			return super.findGame(id);
		} else {
			System.out.println("Failed to get Lock");
			throw new NoLockException();
		}
	}
	
	public boolean updateGame(Game game) {
		if (elm.hasLock(game, user)) {
			super.updateGame(game);
			elm.releaseLock(game, user);
			return true;
		}
		return false;
	}
	
	public void abandonTransaction(Game game, User user) {
		elm.releaseLock(game, user);
	}
}
