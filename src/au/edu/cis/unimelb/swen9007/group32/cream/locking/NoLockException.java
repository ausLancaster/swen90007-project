package au.edu.cis.unimelb.swen9007.group32.cream.locking;

public class NoLockException extends Exception {
	public NoLockException() {
		super("ERROR: Lock not avaliable");
	}
}
