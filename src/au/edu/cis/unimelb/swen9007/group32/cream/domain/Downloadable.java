package au.edu.cis.unimelb.swen9007.group32.cream.domain;

public class Downloadable extends Media {
	private String downloadURL;
	public Downloadable(int id, String downloadURL) {
		super(id);
		this.downloadURL = downloadURL;
	}
	
	public String getDownloadURL() {
		return downloadURL;
	}

	@Override
	public String getMediaType() {
		// TODO Auto-generated method stub
		return "Downloadable";
	}

}
