package au.edu.cis.unimelb.swen9007.group32.cream.data_access;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import au.edu.cis.unimelb.swen9007.group32.cream.domain.Media;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.PhysicalMedia;

public class PhysicalMediaMapper extends AbstractMediaMapper {
	private final static String insertPhysicalMedia = "INSERT INTO APP.physicalmedia VALUES (?, ?, ?)";
	private final static String searchPhysicalMedia = "SELECT * FROM APP.physicalmedia WHERE id = ?";
	
	public PhysicalMediaMapper() {
		super();
	}

	public PhysicalMedia load(int id) {
		PreparedStatement stm = DBConnection.prepare(searchPhysicalMedia);
		try {
			stm.setInt(1, id);
			ResultSet rs = stm.executeQuery();
			while (rs.next()) {
				int pm_id = rs.getInt("id");
				float packagingSize = rs.getFloat("packagingSize");
				String format = rs.getString("format");
				return new PhysicalMedia(pm_id, packagingSize, format);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void save(Media media) {
		PhysicalMedia media2 = (PhysicalMedia) media;
		int id = media2.getId();
		String format = media2.getFormat();
		float packSize = media2.getPackagingSize();
		PreparedStatement stm = DBConnection.prepare(insertPhysicalMedia);
		try {
			stm.setInt(1, id);
			stm.setFloat(2, packSize);
			stm.setString(3, format);
			stm.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
