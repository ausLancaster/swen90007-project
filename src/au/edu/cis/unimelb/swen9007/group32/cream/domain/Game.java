package au.edu.cis.unimelb.swen9007.group32.cream.domain;

import org.eclipse.jdt.internal.compiler.ast.ThisReference;

import au.edu.cis.unimelb.swen9007.group32.cream.data_access.GameMapper;
import au.edu.cis.unimelb.swen9007.group32.cream.distribution.DeveloperDTO;
import au.edu.cis.unimelb.swen9007.group32.cream.distribution.GameDTO;
import au.edu.cis.unimelb.swen9007.group32.cream.distribution.MalformedDTOException;
import au.edu.cis.unimelb.swen9007.group32.cream.distribution.MediaDTO;

public class Game {
	private int id;
	private String title;
	private String description;
	private String imagePath;
	private Developer developer;
	private Media media;
	
	// Stores the Revision ID
	int revId;

	private Float price;
	private Integer qty;

	public Game() {}

	public static GameDTO toDTO(Game game) {
		DeveloperDTO devDTO = Developer.toDTO(game.getDeveloper());
		MediaDTO mediaDTO = Media.toDTO(game.getMedia());
		return new GameDTO(game.id, devDTO, game.getTitle(), game.getDescription(), game.getImagePath(),
				game.getPrice(), game.getQty(), mediaDTO);
	}

	public static Game fromDTO(GameDTO dto) throws MalformedDTOException {
		return new Game(dto.getId(), Developer.fromDTO(dto.getDeveloper()), dto.getTitle(), dto.getDescription(),
				dto.getImagePath(), dto.getPrice(), dto.getQty(), Media.fromDTO(dto.getMedia()));
	}
	
	// Constructor for lazy loading
	public Game(int id, String title) {
		this.id = id;
		this.title = title;
		this.description = null;
		this.imagePath = null;
		this.developer = null;
		this.media = null;
		this.price = null;
		this.qty = null;
	}

	public Game(int id, Developer developer, String title, String description, String imagePath, float price, int qty,
			Media media) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.imagePath = imagePath;
		this.developer = developer;
		this.price = price;
		this.qty = qty;
		this.media = media;
	}

	private void loadObject() {
		GameMapper gameMap = new GameMapper();
		// Initialise everything from memory
		System.out.println(this.getId());
		System.out.println(gameMap);
		Game g = gameMap.findGame(this.getId());
		this.imagePath = g.imagePath;
		this.qty = g.qty;
		this.description = g.description;
		this.title = g.title;
		this.developer = g.developer;
		this.price = g.price;
		this.media = g.media;
		this.qty = g.qty;
	}

	public int getId() {
		return id;
	}

	public Media getMedia() {
		if (this.media == null) {
			// Load the object
			loadObject();
		}
		return media;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		// Fetch the title
		if (this.title == null) {
			// Fetch the title
			loadObject();
		}
		return title;
	}

	public void setTitle(String title) {
		if (this.title == null) {
			loadObject();
		}
		this.title = title;
	}

	public String getDescription() {
		if (this.description == null) {
			// Create the object
			loadObject();
		}
		return description;
	}

	public void setDescription(String description) {
		if (this.description == null) {
			// Create the object
			loadObject();
		}
		this.description = description;
	}

	public String getImagePath() {
		if (this.imagePath == null) {
			loadObject();
		}
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		if (this.imagePath == null) {
			loadObject();
		}
		this.imagePath = imagePath;
	}

	public Developer getDeveloper() {
		if (this.developer == null) {
			loadObject();
		}
		return developer;
	}

	public void setDeveloper(Developer developer) {
		if (this.developer == null) {
			loadObject();
		}
		this.developer = developer;
	}

	public float getPrice() {
		if (this.price == null) {
			loadObject();
		}
		return price;
	}

	public void setPrice(float price) {
		if (this.price == null) {
			loadObject();
		}
		this.price = price;
	}

	public int getQty() {
		if (this.qty == null) {
			loadObject();
		}
		return qty;
	}

	public void setQty(int qty) {
		if (this.qty == null) {
			loadObject();
		}
		this.qty = qty;
	}

	public void buy(int qty) throws GameUnavaliableException, QuantityUnavaliableException {
		if (this.qty == null) {
			loadObject();
		}
		if (this.qty > 0) {
			if (this.qty - qty >= 0) {
				this.qty -= qty;
				return;
			} else {
				throw new QuantityUnavaliableException();
			}
		}
		throw new GameUnavaliableException();
	}

	@Override
	public String toString() {
		return "Game [id=" + id + ", title=" + title + ", description=" + description + ", imagePath=" + imagePath
				+ ", developer=" + developer + ", price=" + price + ", qty=" + qty + "]";
	}

}
