package au.edu.cis.unimelb.swen9007.group32.cream.controller;

import java.io.IOException;

import javax.servlet.ServletException;

import au.edu.cis.unimelb.swen9007.group32.cream.domain.Game;

public class GameCommand extends FrontCommand{

	@Override
	public void process() throws ServletException, IOException {
		forward("/Game.jsp");
	}

}
