package au.edu.cis.unimelb.swen9007.group32.cream.domain;

import au.edu.cis.unimelb.swen9007.group32.cream.distribution.DeveloperDTO;
import au.edu.cis.unimelb.swen9007.group32.cream.distribution.PaymentDTO;

public class Payment {
	public final static String CREDIT_CARD = "Credit-Card";

	private float amount;
	private String method;
	
	public Payment() {}
	
	public Payment(float amount, String method) {
		this.amount = amount;
		this.method = method;
	}
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	
	public static PaymentDTO toDTO(Payment pay) {
		return new PaymentDTO(pay.amount, pay.method);
	}
	
	public static Payment fromDTO(PaymentDTO dto) {
		return new Payment(dto.getAmount(), dto.getMethod());
	}

}
