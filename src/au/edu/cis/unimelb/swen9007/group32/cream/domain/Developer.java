package au.edu.cis.unimelb.swen9007.group32.cream.domain;

import java.util.ArrayList;

import au.edu.cis.unimelb.swen9007.group32.cream.distribution.DeveloperDTO;

public class Developer extends User{

	private String bio;

	public Developer(int id, String name, String email, String password, String address, String bio) {
		super(id, name, email, password, address);
		this.bio = bio;
	}
	
	public static DeveloperDTO toDTO(Developer dev) {
		return new DeveloperDTO(dev.getId(), dev.getName(), dev.getEmail(), dev.getPassword(), dev.getAddress(), dev.getBio());
	}
	
	public static Developer fromDTO(DeveloperDTO dto) {
		return new Developer(dto.getId(), dto.getName(), dto.getEmail(), dto.getPassword(), dto.getAddress(), dto.getBio());
	}

	public Developer() {
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}
	
	public ArrayList<Game> getAllDevelopedGames() {
		ServiceLayer sl = new ServiceLayer();
		ArrayList<Game> retGames = new ArrayList<Game>();
		ArrayList<Game> games = (ArrayList<Game>) sl.listAllGames();
		for (Game game : games) {
			if (game.getDeveloper().getId() == this.getId()) {
				retGames.add(game);
			}
		}
		return retGames;
	}

}
