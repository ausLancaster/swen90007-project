package au.edu.cis.unimelb.swen9007.group32.cream.domain;

import au.edu.cis.unimelb.swen9007.group32.cream.distribution.MalformedDTOException;
import au.edu.cis.unimelb.swen9007.group32.cream.distribution.PurchaseDTO;

public class Purchase {
	
	public Purchase(int id, Payment payment, Game game, int qty) {
		this.id = id;
		this.payment = payment;
		this.game = game;
		this.qty = qty;
	}
	
	public Purchase() {} 
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Payment getPayment() {
		return payment;
	}
	public void setPayment(Payment payment) {
		this.payment = payment;
	}
	public Game getGame() {
		return game;
	}
	public void setGame(Game game) {
		this.game = game;
	}
	public int getQty() {
		return this.qty;
	}
	private int id;
	private Payment payment;
	private Game game;
	private int qty;
	
	public static PurchaseDTO toDTO(Purchase p) {
		return new PurchaseDTO(p.getId(), Payment.toDTO(p.getPayment()), Game.toDTO(p.getGame()), p.getQty());
	}
	
	public static Purchase fromDTO(PurchaseDTO dto) throws MalformedDTOException {
		return new Purchase(dto.getId(), Payment.fromDTO(dto.getPayment()), Game.fromDTO(dto.getGame()), dto.getQty());
	}
}
