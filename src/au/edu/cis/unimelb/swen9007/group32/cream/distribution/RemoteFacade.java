package au.edu.cis.unimelb.swen9007.group32.cream.distribution;

import java.beans.XMLEncoder;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.rmi.RemoteException;
import java.util.ArrayList;

import au.edu.cis.unimelb.swen9007.group32.cream.domain.Customer;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.Game;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.GameUnavaliableException;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.Payment;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.QuantityUnavaliableException;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.ServiceLayer;

public class RemoteFacade {

	// These functions allow for the Manipulation of Game Objects Remotely
	public GameDTO getGameDTO(int id) throws RemoteException {
		ServiceLayer sl = new ServiceLayer();
		Game game = sl.findGame(id);
		return Game.toDTO(game);
	}
	
	public ArrayList<GameDTO> getAllGames() {
		ArrayList<GameDTO> games = new ArrayList<GameDTO>();
		ServiceLayer sl = new ServiceLayer();
		ArrayList<Game> allGames = new ArrayList<Game>();
		allGames = (ArrayList<Game>) sl.listAllGames();
		
		for (Game game : allGames) {
			games.add(Game.toDTO(game));
		}
		return games;
	}
	
	/** Returns a List of All games in XML Formatting */
	public String getAllGamesXML() {
		ArrayList<String> serialisedGames = new ArrayList<String>();
		ArrayList<GameDTO> allGames = getAllGames();
		for (GameDTO g : allGames) {
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			GameDTO.toXML(g, os);
			serialisedGames.add(os.toString());
		}
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
	
		XMLEncoder encoder = new XMLEncoder(outputStream);
		encoder.writeObject(allGames);
		encoder.close();
		return outputStream.toString();
	}

	public String getGameXML(int id) throws RemoteException {
		GameDTO gameDTO = getGameDTO(id);
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		GameDTO.toXML(gameDTO, os);
		return os.toString();
	}

	public Boolean updateGame(String xml) {
		InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
		GameDTO gDTO = GameDTO.fromXML(is);
		ServiceLayer sl = new ServiceLayer();
		try {
			sl.updateGame(Game.fromDTO(gDTO));
			return true;
		} catch (MalformedDTOException e) {
			return false;
		}
	}
	
	// These functions allow for purchases to be made remotely
	public String makePayment(float amount, String method) {
		Payment payment = ServiceLayer.makePayment(amount, method);
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		PaymentDTO pDTO = Payment.toDTO(payment);
		PaymentDTO.toXML(pDTO, os);
		return os.toString();
	}
	
	/** Allows for purchases to be made remotely */
	public boolean makePurchase(String paymentXML, String customerXML, String gameXML, int qty) {
		ServiceLayer sl = new ServiceLayer();
		
		InputStream isPaymentXML = new ByteArrayInputStream(paymentXML.getBytes(StandardCharsets.UTF_8));
		Payment payment = Payment.fromDTO(PaymentDTO.fromXML(isPaymentXML));
		
		InputStream isCustomerXML = new ByteArrayInputStream(customerXML.getBytes(StandardCharsets.UTF_8));
		Customer c = Customer.fromDTO(CustomerDTO.fromXML(isPaymentXML));
		
		InputStream isGameXML = new ByteArrayInputStream(gameXML.getBytes(StandardCharsets.UTF_8));
		Game game;
		try {
			game = Game.fromDTO(GameDTO.fromXML(isGameXML));
			sl.buyGame(c, qty, payment, game);
			return true;
		} catch (MalformedDTOException e) {
			return false;
		} catch (GameUnavaliableException e) {
			return false;
		} catch (QuantityUnavaliableException e) {
			return false;
		}
	}
	
	public CustomerDTO getCustomerDTO(int id) {
		ServiceLayer sl = new ServiceLayer();
		Customer customer = sl.findCustomer(id);
		return Customer.toDTO(customer);
	}
	
	public String getCustomerXML(int id) {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		CustomerDTO cDTO = getCustomerDTO(id);
		CustomerDTO.toXML(cDTO, os);
		return os.toString();
	}

	public void updateCustomerDTO(CustomerDTO custDTO) {
		ServiceLayer sl = new ServiceLayer();
		Customer customer = Customer.fromDTO(custDTO);
		sl.updateCustomer(customer);
	}
	
	public void XMLupdateCustomer(String xml) {
		InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
		CustomerDTO cDTO = CustomerDTO.fromXML(is);
		updateCustomerDTO(cDTO);
	}
	

}
