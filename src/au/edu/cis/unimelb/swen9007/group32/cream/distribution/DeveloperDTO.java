package au.edu.cis.unimelb.swen9007.group32.cream.distribution;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.InputStream;
import java.io.OutputStream;

public class DeveloperDTO {
	private int id;
	private String name;
	private String email;
	private String password;
	private String address;
	private String bio;
	
	public DeveloperDTO(int id, String name, String email, String password, String address, String bio) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.password = password;
		this.address = address;
		this.bio = bio;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getBio() {
		return bio;
	}
	public void setBio(String bio) {
		this.bio = bio;
	}
	
	public static void toXML(DeveloperDTO devDTO, OutputStream outputStream) {
		XMLEncoder encoder = new XMLEncoder(outputStream);
		encoder.writeObject(devDTO);
		encoder.close();
	}
	
	public static DeveloperDTO fromXML(InputStream inputStream) {
		XMLDecoder decoder = new XMLDecoder(inputStream);
		DeveloperDTO devDTO = (DeveloperDTO) decoder.readObject();
		decoder.close();
		return devDTO;
	}
}
