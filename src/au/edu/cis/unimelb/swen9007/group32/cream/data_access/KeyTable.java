package au.edu.cis.unimelb.swen9007.group32.cream.data_access;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class KeyTable {
	public static Integer getKey(String tableName) {
		String query = "SELECT nextID FROM APP.keys WHERE tablename = ?";
		String update = "UPDATE APP.keys SET nextID = ? WHERE tablename = ?";
		PreparedStatement preqQuery = null;
		PreparedStatement updateStm = null;

		preqQuery = DBConnection.prepare(query);
		try {
			preqQuery.setString(1, tableName);
			ResultSet rs = preqQuery.executeQuery();
			
			rs.next();
			int val = rs.getInt(1);
			int nextKey = val + 1;

			updateStm = DBConnection.prepare(update);
			updateStm.setInt(1, nextKey);
			updateStm.setString(2, tableName);
			updateStm.executeUpdate();
			return val;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
