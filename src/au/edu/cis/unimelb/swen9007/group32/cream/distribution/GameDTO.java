package au.edu.cis.unimelb.swen9007.group32.cream.distribution;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.InputStream;
import java.io.OutputStream;

import au.edu.cis.unimelb.swen9007.group32.cream.domain.Developer;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.Media;

public class GameDTO {
	private int id;
	private String title;
	private String description;
	private String imagePath;
	private Float price;
	private Integer qty;

	private DeveloperDTO developer;
	private MediaDTO media;
	
	public GameDTO() {}
	
	public GameDTO (int id, DeveloperDTO developer, String title, String description, String imagePath, float price, int qty, MediaDTO media) {
		this.id = id;
		this.developer = developer;
		this.title = title;
		this.description = description;
		this.imagePath = imagePath;
		this.price = price;
		this.qty = qty;
		this.media = media;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public DeveloperDTO getDeveloper() {
		return developer;
	}

	public void setDeveloper(DeveloperDTO developer) {
		this.developer = developer;
	}

	public MediaDTO getMedia() {
		return media;
	}

	public void setMedia(MediaDTO media) {
		this.media = media;
	}
	
	public static void toXML(GameDTO gameDTO, OutputStream outputStream) {
		XMLEncoder encoder = new XMLEncoder(outputStream);
		encoder.writeObject(gameDTO);
		encoder.close();
	}
	
	public static GameDTO fromXML(InputStream inputStream) {
		XMLDecoder decoder = new XMLDecoder(inputStream);
		GameDTO gameDTO = (GameDTO) decoder.readObject();
		decoder.close();
		return gameDTO;
	}

}
