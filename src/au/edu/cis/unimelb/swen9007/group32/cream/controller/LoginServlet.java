package au.edu.cis.unimelb.swen9007.group32.cream.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

import au.edu.cis.unimelb.swen9007.group32.cream.domain.Customer;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.Developer;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.ServiceLayer;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.User;
import au.edu.cis.unimelb.swen9007.group32.cream.session.AppSession;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private String view = "/Login.jsp";
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ServletContext servletContext = getServletContext();
		RequestDispatcher rd = (RequestDispatcher) servletContext.getRequestDispatcher(view);
		rd.forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String email = request.getParameter("email");
		System.out.println("Email " + email);
		
		String password = request.getParameter("password");
		System.out.println("password " + password);

		//UsernamePasswordToken token = new UsernamePasswordToken(email, password);
		Subject currentUser = SecurityUtils.getSubject();
		String view = "/Home.jsp";
		
		UsernamePasswordToken token = new UsernamePasswordToken(email, password); 
		try {
			currentUser.login(token);
			ServiceLayer sl = new ServiceLayer();
			User user;
			System.out.println("Email " + email);
			if (currentUser.hasRole(AppSession.DEVELOPER_ROLE) ) {
				// Create a new Developer
				user = sl.getDeveloperFromEmail(email);
				AppSession.init(user);
			} else if (currentUser.hasRole(AppSession.CUSTOMER_ROLE) ) {
				// Create a new Customer
				user = sl.getCustomerFromEmail(email);
				AppSession.init(user);
			}
		} catch  ( UnknownAccountException | IncorrectCredentialsException e ) {
			System.out.println("Account does not exist");
			request.setAttribute("error", "Username Or Password Doesn't exist");
			view = "/Login.jsp";
		} finally {
			ServletContext servletContext = getServletContext();
			RequestDispatcher rd = (RequestDispatcher) servletContext.getRequestDispatcher(view);
			rd.forward(request, response);
		}
	}
}
