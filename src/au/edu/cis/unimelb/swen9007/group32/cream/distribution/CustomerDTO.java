package au.edu.cis.unimelb.swen9007.group32.cream.distribution;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.InputStream;
import java.io.OutputStream;

public class CustomerDTO {
	private int id;
	private String name;
	private String password;
	private String email;
	private String address;
	
	public CustomerDTO() {}
	
	public CustomerDTO(int id, String name, String password, String email, String address) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.address = address;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public static void toXML(CustomerDTO custDTO, OutputStream outputStream) {
		XMLEncoder encoder = new XMLEncoder(outputStream);
		encoder.writeObject(custDTO);
		encoder.close();
	}
	
	public static CustomerDTO fromXML(InputStream inputStream) {
		XMLDecoder decoder = new XMLDecoder(inputStream);
		CustomerDTO custDTO = (CustomerDTO) decoder.readObject();
		decoder.close();
		return custDTO;
	}
}
