package au.edu.cis.unimelb.swen9007.group32.cream.distribution;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.InputStream;
import java.io.OutputStream;

public class MediaDTO {
	public static final String DOWNLOADABLE = "Downloadable";
	public static final String PHYSICAL = "Physical";
	private Integer id = null;
	private String type = null;
	private String URL = null;
	private Float packagingSize = null;
	private String format = null;
	
	public MediaDTO() {}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}

	public Float getPackagingSize() {
		return packagingSize;
	}

	public void setPackagingSize(float packagingSize) {
		this.packagingSize = packagingSize;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public Integer getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public MediaDTO(int id, String url) {
		this.type = DOWNLOADABLE;
		this.id = id;
		this.URL = url;
	}
	
	public MediaDTO(int id, float packagingSize, String format) {
		this.type = PHYSICAL;
		this.id = id;
		this.packagingSize = packagingSize;
		this.format = format;
	}
	
	public static void toXML(MediaDTO mediaDto, OutputStream outputStream) {
		XMLEncoder encoder = new XMLEncoder(outputStream);
		encoder.writeObject(mediaDto);
		encoder.close();
	}

	public static MediaDTO fromXML(InputStream inputStream) {
		XMLDecoder decoder = new XMLDecoder(inputStream);
		MediaDTO mediaDto = (MediaDTO) decoder.readObject();
		decoder.close();
		return mediaDto;
	}
}
