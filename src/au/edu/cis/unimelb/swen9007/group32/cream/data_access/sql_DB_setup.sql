CREATE SCHEMA APP;
DROP TABLE APP.customerpurchase;
DROP TABLE APP.purchases;
DROP TABLE APP.games;
DROP TABLE APP.developers;
DROP TABLE APP.customers;
DROP TABLE APP.keys;
DROP TABLE APP.downloadablemedia;
DROP TABLE APP.physicalmedia;
DROP TABLE APP.gameLock;

CREATE TABLE APP.developers (
	id		INT,
    name    VARCHAR(50),
    email   VARCHAR(100) UNIQUE,
    password VARCHAR(50),
    address VARCHAR(200),
	bio		VARCHAR(500),
	PRIMARY KEY (id)
);

CREATE TABLE APP.downloadablemedia (
    id      INT NOT NULL,
	URL		VARCHAR(200) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE APP.gameLock (
	gameID			INT NOT NULL,
	owner			VARCHAR(50),
	PRIMARY KEY(gameID)
);

CREATE TABLE APP.physicalmedia (
    id          	 INT NOT NULL,
	packagingSize    FLOAT DEFAULT NULL,
	format			 VARCHAR(50) DEFAULT NULL,
	PRIMARY KEY(id)
);

INSERT INTO APP.downloadablemedia VALUES (1, 'http://fungames.invalid/game1');
INSERT INTO APP.downloadablemedia VALUES (2, 'http://fungames.invalid/game2');
INSERT INTO APP.downloadablemedia VALUES (3, 'http://fungames.invalid/game3');

INSERT INTO APP.physicalmedia VALUES (4, 10.5, 'DVD');
INSERT INTO APP.physicalmedia VALUES (5, 3.25, 'Floppy disk');

INSERT INTO APP.developers
VALUES (001, 'Agile Dingo Productions', 'agile.dinge@email.com','password','15 Woodstock Road Maryblong', 'bio');
INSERT INTO APP.developers
VALUES (002, 'Bioflux Entertainment', 'bioflux@hotmail.com','password2','27 Harrying Street Plelbourne', 'bio');

CREATE TABLE APP.games(
   id   INT,
   developerID INT,
   title  VARCHAR(50),
   description VARCHAR(500),
   imagePath VARCHAR(50),
   price  FLOAT,
   qty INT,
   mediaId INT,
   FOREIGN KEY (developerID) REFERENCES APP.developers (id),
   PRIMARY KEY (id)
);

INSERT INTO APP.games VALUES (1001, 001, 'Mass Assault', 'Description', '/pictures/massassault.png', 11.11, 11, 1);
INSERT INTO APP.games VALUES (1002, 001, 'Disorder and Treason', 'Description', '/pictures/disorder.png', 22.22, 22, 2);
INSERT INTO APP.games VALUES (1003, 002, 'Defeat of Retribution', 'Description', '/pictures/defeat.png', 33.33, 33, 3);
INSERT INTO APP.games VALUES (1004, 002, 'Deaddroid', 'Description', '/pictures/deaddroid.png', 44.44, 44, 4);
INSERT INTO APP.games VALUES (1005, 002, 'Rubble and Reckoning', 'Description', '/pictures/rubble.png', 55.55, 55, 5);

CREATE TABLE APP.customers (
   id      INT,
   name    VARCHAR(50),
   email   VARCHAR(100) UNIQUE,
   password VARCHAR(50),
   address VARCHAR(200),
   PRIMARY KEY(id));

INSERT INTO APP.customers
VALUES (001, 'Albert Einstein', 'albert.einstein@ias.edu', 'verySmart', 'Office 9, IAS, Princeton, USA');
INSERT INTO APP.customers
VALUES (007, 'James Bond', 'jbond@ghcq.uk', 'bondJames', 'Queen Street 9, Chelsea 338, London');

CREATE TABLE APP.purchases (
   id         INT,
   gameID     INT,
   quantity   INT,
   paymentMethod VARCHAR(20),
   paymentAmount FLOAT,
   PRIMARY KEY (id),
   FOREIGN KEY (gameID) REFERENCES APP.games (id)
);

CREATE TABLE APP.customerpurchase (
   customerID INT,
   purchaseID INT,
   PRIMARY KEY (customerID, purchaseID),
   FOREIGN KEY (customerID) REFERENCES APP.customers (id),
   FOREIGN KEY (purchaseID) REFERENCES APP.purchases (id)
);


CREATE TABLE APP.keys (
	tableName VARCHAR(50) NOT NULL,
	nextId    INT DEFAULT 0,
	PRIMARY KEY(tableName)
);

INSERT INTO APP.keys VALUES ('purchases', 0);
INSERT INTO APP.keys VALUES ('media', 6);
