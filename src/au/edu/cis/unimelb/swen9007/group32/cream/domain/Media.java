package au.edu.cis.unimelb.swen9007.group32.cream.domain;

import au.edu.cis.unimelb.swen9007.group32.cream.data_access.KeyTable;
import au.edu.cis.unimelb.swen9007.group32.cream.distribution.MalformedDTOException;
import au.edu.cis.unimelb.swen9007.group32.cream.distribution.MediaDTO;

public abstract class Media {
	int mediaId;
	
	public Media() {}

	public static int generateUniquePK() {
		return KeyTable.getKey("media");
	}

	public Media(int id) {
		this.mediaId = id;
	}

	public int getId() {
		return mediaId;
	}

	public abstract String getMediaType();

	public static MediaDTO toDTO(Media media) {
		if (media instanceof Downloadable) {
			return new MediaDTO(media.getId(), ((Downloadable) media).getDownloadURL());
		} else if (media instanceof PhysicalMedia) {
			return new MediaDTO(media.getId(), ((PhysicalMedia) media).getPackagingSize(),
					((PhysicalMedia) media).getFormat());
		}
		return null;
	}

	public static Media fromDTO(MediaDTO dto) throws MalformedDTOException {
		if (dto.getType() == MediaDTO.DOWNLOADABLE) {
			if (dto.getId() != null && dto.getURL() != null) {
				Downloadable dl = new Downloadable(dto.getId(), dto.getURL());
				return dl;
			}
		} else if (dto.getType() == MediaDTO.PHYSICAL) {
			if (dto.getId() != null && dto.getPackagingSize() != null && dto.getFormat() != null) {
				PhysicalMedia pm = new PhysicalMedia(dto.getId(), dto.getPackagingSize(), dto.getFormat());
				return pm;
			}
		}
		throw new MalformedDTOException();
	}
}
