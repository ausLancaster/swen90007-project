package au.edu.cis.unimelb.swen9007.group32.cream.domain;

public class QuantityUnavaliableException extends Exception{
	public QuantityUnavaliableException() {
		super("The game is not avaliable in the quantity selected");
	}
}
