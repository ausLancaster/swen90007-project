package au.edu.cis.unimelb.swen9007.group32.cream.domain;

import java.util.ArrayList;

import au.edu.cis.unimelb.swen9007.group32.cream.data_access.GameUnitOfWork;
import au.edu.cis.unimelb.swen9007.group32.cream.data_access.KeyTable;
import au.edu.cis.unimelb.swen9007.group32.cream.data_access.PurchaseMapper;
import au.edu.cis.unimelb.swen9007.group32.cream.distribution.CustomerDTO;
import au.edu.cis.unimelb.swen9007.group32.cream.locking.LockingGameMapper;

public class Customer extends User{

	public Customer(int id, String name, String email, String password, String address) {
		super(id, name, email, password, address);
	}
	
	public Customer() {
		super();
	}
	
	public ArrayList<Purchase> getPurchases() {
		return null;
	}
	
	public static Customer fromDTO(CustomerDTO dto) {
		return new Customer(dto.getId(), dto.getName(), dto.getPassword(), dto.getEmail(), dto.getAddress());
	}
	
	public static CustomerDTO toDTO(Customer customer) {
		return new CustomerDTO(customer.getId(), customer.getName(), customer.getPassword(), customer.getEmail(), customer.getAddress());
	}

	public void performPurchase(Game game, Payment payment, int qty) throws GameUnavaliableException, QuantityUnavaliableException {
		PurchaseMapper purchaseMapper = new PurchaseMapper();
		LockingGameMapper lgm = new LockingGameMapper(this);
		GameUnitOfWork guow = GameUnitOfWork.getCurrent();
		if (guow == null) {
			GameUnitOfWork.newCurrent();
			guow = GameUnitOfWork.getCurrent();
		}
		
		game.buy(qty);
		System.out.println("New Game QTY " + game.getQty());
		guow.registerDirty(game);
		guow.commit(lgm);
		
		// Make the purchase id
		int purchaseId = KeyTable.getKey("purchases");
		
		// Create the purchase, associated with the payment.
		Purchase purchase = new Purchase(purchaseId, payment, game, qty);
		purchaseMapper.create(purchase, this);
	}

}
