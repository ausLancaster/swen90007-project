package au.edu.cis.unimelb.swen9007.group32.cream.data_access;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import au.edu.cis.unimelb.swen9007.group32.cream.domain.Customer;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.Purchase;


public class CustomerMapper {
	private final static String findCustomer = "SELECT * from APP.customers WHERE id = ?";
	private final static String updateCustomer = "UPDATE APP.customers SET name=?, email=?, address=? WHERE id=?;";
	private final static String findCustomerByEmail = "SELECT * from APP.customers WHERE email = ?";
	private final static String findPurchases = "SELECT purchaseID FROM APP.customerpurchase WHERE customerID = ?";
	
	public Customer lookupCustomer(int customerId) {
		PreparedStatement sqlPrepared = DBConnection.prepare(findCustomer);
		try {
			sqlPrepared.setInt(1, customerId);
			ResultSet rs = sqlPrepared.executeQuery();
			while(rs.next()) {
				int id = rs.getInt(1);
				String name = rs.getString(2);
				String email = rs.getString(3);
				String password = rs.getString(4);
				String address = rs.getString(5);
				return new Customer(id, name, email, password, address);		
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public void updateCustomer(Customer customer) {
		PreparedStatement sqlPrepared = DBConnection.prepare(updateCustomer);
		try {
			sqlPrepared.setString(1, customer.getName());
			sqlPrepared.setString(2, customer.getEmail());
			sqlPrepared.setString(3, customer.getAddress());
			sqlPrepared.setInt(4, customer.getId());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public Customer lookupCustomerByEmail(String email) {
		PreparedStatement sqlPrepared = DBConnection.prepare(findCustomerByEmail);
		try {
			sqlPrepared.setString(1, email);
			ResultSet rs = sqlPrepared.executeQuery();
			while(rs.next()) {
				int id = rs.getInt(1);
				String name = rs.getString(2);
				String password = rs.getString(4);
				String address = rs.getString(5);
				return new Customer(id, name, email, password, address);		
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/** Returns all the purchases associated with a customer */
	public ArrayList<Purchase> getPurchases(Customer customer) {
		ArrayList<Purchase> purchases = new ArrayList<Purchase>();
		PreparedStatement sqlPrepared = DBConnection.prepare(findPurchases);
		try {
			PurchaseMapper pm = new PurchaseMapper();
			sqlPrepared.setInt(1, customer.getId());
			ResultSet rs = sqlPrepared.executeQuery();
			while(rs.next()) {
				purchases.add(pm.getPurchase(rs.getInt("purchaseID")));
			}
			return purchases;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
