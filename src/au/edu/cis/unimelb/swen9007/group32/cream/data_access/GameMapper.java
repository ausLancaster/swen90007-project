package au.edu.cis.unimelb.swen9007.group32.cream.data_access;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.*;

public class GameMapper {
	private final static String updateGame = "UPDATE APP.games SET developerID = ?, title=?, description=?, imagePath=?, price=?, qty=? WHERE id=?";
	private final static String getAllString = "SELECT * from  APP.games";
	private final static String findString = "SELECT * from APP.games WHERE id = ?";
	private final static String insertGameQry = "INSERT INTO APP.games VALUES (?, ?, ?, ?, ?, ?, ?)";
	private final static String removeGameQry = "DELETE FROM APP.games WHERE id = ?";
	private final static String searchQry = "SELECT * FROM APP.games WHERE LOWER(TITLE) LIKE LOWER(?)";

	public List<Game> listAllGames() {
		IdentityMap<Game> gameIdMap = IdentityMap.getInstance(new Game());
		ArrayList<Game> games = new ArrayList<Game>();
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			statement = DBConnection.prepare(getAllString);
			rs = statement.executeQuery();
			while (rs.next()) {
				Game g = gameIdMap.get(rs.getInt("id"));
				if (g == null) {
					g = new Game(rs.getInt("id"), rs.getString("title"));
					gameIdMap.put(g.getId(), g);
				}
				games.add(g);

			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return games;
	}

	public ArrayList<Game> searchGame(String term) {
		IdentityMap<Game> gameIdMap = IdentityMap.getInstance(new Game());
		IdentityMap<Developer> devIdMap = IdentityMap.getInstance(new Developer());
		term = "%" + term + "%";
		DeveloperMapper devMapper = new DeveloperMapper();
		ArrayList<Game> games = new ArrayList<Game>();
		PreparedStatement statement = null;
		statement = DBConnection.prepare(searchQry);
		try {
			statement.setString(1, term);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				Game g = gameIdMap.get(rs.getInt("id"));
				if (g == null) {
					int devId = rs.getInt("developerID");
					Developer dev = devIdMap.get(devId);
					if (dev == null) {
						dev = devMapper.getDeveloper(devId);
						devIdMap.put(dev.getId(), dev);
					}
					g = new Game(rs.getInt("id"), rs.getString("title"));
					gameIdMap.put(g.getId(), g);
				}
				if (g != null) {
					games.add(g);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return games;
	}

	public Game findGame(int id) {
		IdentityMap<Developer> devIdMap = IdentityMap.getInstance(new Developer());
		PreparedStatement statement = null;
		ResultSet rs = null;
		DeveloperMapper developerMapper = new DeveloperMapper();
		MediaMapper mediaMapper = new MediaMapper();
		Game g = null;
		try {
			statement = DBConnection.prepare(findString);
			statement.setInt(1, id);
			rs = statement.executeQuery();
			if (rs.next()) {
				int devId = rs.getInt("developerID");
				Developer dev = devIdMap.get(devId);
				if (dev == null) {
					dev = developerMapper.getDeveloper(devId);
					devIdMap.put(dev.getId(), dev);
				}
				Media gameMedia = mediaMapper.getMedia(rs.getInt("mediaId"));
				g = new Game(rs.getInt("id"), dev, rs.getString("title"), rs.getString("description"),
						rs.getString("imagePath"), rs.getFloat("price"), rs.getInt("qty"), gameMedia);
			} else {
				System.out.println("Game with id " + id + " not found");
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return g;
	}

	public boolean updateGame(Game game) {
		// Update the game
		PreparedStatement gameUpdate = DBConnection.prepare(updateGame);
		try {
			// Check the Game developer
			DeveloperMapper devMapper = new DeveloperMapper();
			if (devMapper.getDeveloper(game.getDeveloper().getId()).equals(null)) {
				devMapper.createDeveloper(game.getDeveloper());
			}
			
			IdentityMap<Game> gameIdMap = IdentityMap.getInstance(game);
			gameUpdate.setInt(1, game.getDeveloper().getId());
			gameUpdate.setString(2, game.getTitle());
			gameUpdate.setString(3, game.getDescription());
			gameUpdate.setString(4, game.getImagePath());
			gameUpdate.setFloat(5, game.getPrice());
			gameUpdate.setInt(6, game.getQty());
			gameUpdate.setInt(7, game.getId());
			gameUpdate.executeUpdate();
			
			if (gameIdMap.get(game.getId()) != null) {
				gameIdMap.put(game.getId(), game);
			}
			
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public void insertGame(Game game) {
		PreparedStatement insertGame = DBConnection.prepare(insertGameQry);
		try {
			DeveloperMapper devMapper = new DeveloperMapper();
			if (devMapper.getDeveloper(game.getDeveloper().getId()).equals(null)) {
				devMapper.createDeveloper(game.getDeveloper());
			}
			insertGame.setInt(1, game.getId());
			insertGame.setInt(2, game.getDeveloper().getId());
			insertGame.setString(3, game.getTitle());
			insertGame.setString(4, game.getDescription());
			insertGame.setString(5, game.getImagePath());
			insertGame.setFloat(6, game.getPrice());
			insertGame.setInt(7, game.getQty());
			insertGame.setInt(8, game.getMedia().getId());
			insertGame.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void removeGame(Game game) {
		PreparedStatement statement = DBConnection.prepare(removeGameQry);
		try {
			statement.setInt(1, game.getId());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
