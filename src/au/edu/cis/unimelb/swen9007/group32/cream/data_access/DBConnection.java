package au.edu.cis.unimelb.swen9007.group32.cream.data_access;

import java.sql.*;

public class DBConnection {

	// For local setup, uncomment this line or, better, set the environment variable
	// in your run configuration
	// private static final String DB_CONNECTION =
	// "jdbc:derby://localhost:1527/ebookshop;create=true";
	//private static final String DB_CONNECTION = "jdbc:postgresql://ec2-54-227-244-12.compute-1.amazonaws.com:5432/d439v7qefgjhri";
	//private static final String DB_USER = "nmxfutxmjrfark";
	//private static final String DB_PASSWORD = "5d258a0ff2b940bd8e9fe3260121813e3a93e7e97ea792446bc67905cdd61aae";
    private static final String DB_CONNECTION = "jdbc:derby://localhost:1527/cream";
	private static final String DB_USER = "user";
	private static final String DB_PASSWORD = "1234";

	private static Connection dbConnection = null;
	
	public static PreparedStatement prepare(String stm) {
		PreparedStatement preparedStatement = null;
		try {
			if (dbConnection == null) {
				System.out.println(DB_CONNECTION);
				// Used to avoid Heroku restrictions on database connections
				dbConnection = getDBConnection();
				System.out.println(dbConnection);
			}
			preparedStatement = dbConnection.prepareStatement(stm);
		} catch (SQLException e) {
			System.out.println(e.getLocalizedMessage());
		}

		return preparedStatement;
	}

	private static Connection getDBConnection() {

		try {
			DriverManager.registerDriver(new org.apache.derby.jdbc.ClientDriver());
			//DriverManager.registerDriver(new org.postgresql.Driver());
			//DriverManager.registerDriver(new org.apache.derby.jdbc.ClientDriver());

			Connection dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
			return dbConnection;

		} catch (SQLException e) {
			System.out.println(e.getMessage());		
		}
		return null;

	}

	public static PreparedStatement prepare(String stm, int returnGeneratedKeys) {
		PreparedStatement preparedStatement = null;
		try {

			Connection dbConnection = getDBConnection();

			preparedStatement = dbConnection.prepareStatement(stm, Statement.RETURN_GENERATED_KEYS);

		} catch (SQLException e) {

			System.out.println(e.getMessage());

		}

		return preparedStatement;
	}
}
