package au.edu.cis.unimelb.swen9007.group32.cream.auth;

import au.edu.cis.unimelb.swen9007.group32.cream.data_access.CustomerMapper;
import au.edu.cis.unimelb.swen9007.group32.cream.data_access.DeveloperMapper;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.*;
import au.edu.cis.unimelb.swen9007.group32.cream.session.AppSession;

import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.jdbc.JdbcRealm;
import org.apache.shiro.subject.PrincipalCollection;

import java.util.HashSet;
import java.util.Set;

public class AppRealm extends JdbcRealm {

	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		// identify account to log to
		System.out.println("Getting Auth Info");
		UsernamePasswordToken userPassToken = (UsernamePasswordToken) token;
		final String username = userPassToken.getUsername();

		String password;

		CustomerMapper cm = new CustomerMapper();
		ServiceLayer sl = new ServiceLayer();
		final Customer customer = sl.getCustomerFromEmail(username);
		
		if (customer != null) {
			password = customer.getPassword();
			return new SimpleAuthenticationInfo(customer, password, getName());
		}
		
		Developer dev = sl.getDeveloperFromEmail(username);
		
		if (dev != null) {
			password = dev.getPassword();
			return new SimpleAuthenticationInfo(dev, password, getName());
		}
		
		System.out.println("No account found for user with username " + username);
		return null;
	}

	@Override
	protected AuthorizationInfo getAuthorizationInfo(PrincipalCollection principals) {
		Set<String> roles = new HashSet<>();
		if (principals.isEmpty()) {
			System.out.println("Given principals to authorize are empty.");
			return null;
		}

		// Get the Username From the Develper
		String username = (String) ((User)principals.getPrimaryPrincipal()).getEmail();
		CustomerMapper cm = new CustomerMapper();
		final Customer user = cm.lookupCustomerByEmail(username);
		Developer developer = null;
		if (user == null) {
			DeveloperMapper dm = new DeveloperMapper();
			developer = dm.lookupDeveloperByEmail(username);
			System.out.println("Dev " + developer);
			if (developer != null) {
				roles.add(AppSession.DEVELOPER_ROLE);
			}
		} else {
			roles.add(AppSession.CUSTOMER_ROLE);
		}
		if (user == null && developer == null) {
			System.out.println("No account found for user with username " + username);
			return null;
		}

		return new SimpleAuthorizationInfo(roles);
	}
}
