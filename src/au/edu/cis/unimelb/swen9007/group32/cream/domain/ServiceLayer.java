package au.edu.cis.unimelb.swen9007.group32.cream.domain;

import java.util.ArrayList;
import java.util.List;
import au.edu.cis.unimelb.swen9007.group32.cream.data_access.*;

public class ServiceLayer {

	public ServiceLayer() {

		GameUnitOfWork.newCurrent();
	}

	public List<Game> listAllGames() {
		GameMapper gameMapper = new GameMapper();
		return gameMapper.listAllGames();
	}

	public Customer getCustomerFromEmail(String email) {
		CustomerMapper customerMapper = new CustomerMapper();
		Customer customer = customerMapper.lookupCustomerByEmail(email);
		if (customer != null) {
			IdentityMap<Customer> customerMap = IdentityMap.getInstance(customer);

			// Avoid creating multiple customers twice
			int id = customer.getId();
			if (customerMap.get(id) != null) {
				return customerMap.get(id);
			}
		}

		return customer;
	}

	public Developer getDeveloperFromEmail(String email) {
		DeveloperMapper devMapper = new DeveloperMapper();
		Developer dev = devMapper.lookupDeveloperByEmail(email);
		if (dev != null) {
			IdentityMap<Developer> developerIMap = IdentityMap.getInstance(dev);

			if (developerIMap.get(dev.getId()) != null) {
				return developerIMap.get(dev.getId());
			}
		}
		return dev;
	}

	public void updateCustomer(Customer customer) {
		CustomerMapper customerMapper = new CustomerMapper();
		customerMapper.updateCustomer(customer);
	}

	public List<Game> searchGames(String term) {
		GameMapper gameMapper = new GameMapper();
		return gameMapper.searchGame(term);
	}

	public static List<Purchase> getAllCustomerPurchases(Customer customer) {
		CustomerMapper custMap = new CustomerMapper();
		return custMap.getPurchases(customer);
	}


	public Game findGame(int id) {
		GameMapper gameMapper = new GameMapper();
		// Look up game id
		Game game = new Game();
		IdentityMap<Game> gameMap = IdentityMap.getInstance(game);

		game = gameMap.get(id);
		if (game == null) {
			// Load the game from the database
			game = gameMapper.findGame(id);
			if (game != null) {
				gameMap.put(game.getId(), game);
			}
		}
		return game;
	}

	public Customer findCustomer(int id) {
		CustomerMapper customerMapper = new CustomerMapper();
		Customer customer = new Customer();
		IdentityMap<Customer> customerMap = IdentityMap.getInstance(customer);

		customer = customerMap.get(id);
		if (customer == null) {
			customer = customerMapper.lookupCustomer(id);
		}
		return customer;
	}
	

	public static Payment makePayment(float amount, String method) {
		return new Payment(amount, method);
	}

	public void buyGame(Customer c, int qty, Payment payment, Game game)
			throws GameUnavaliableException, QuantityUnavaliableException {
		c.performPurchase(game, payment, qty);
	}
}
