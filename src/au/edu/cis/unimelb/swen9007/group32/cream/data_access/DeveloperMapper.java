package au.edu.cis.unimelb.swen9007.group32.cream.data_access;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import au.edu.cis.unimelb.swen9007.group32.cream.domain.*;

public class DeveloperMapper {
	String developerSearch = "SELECT * FROM APP.developers where id = ?";
	String developerEmailSearch = "SELECT * FROM APP.developers where email like ?";
	String addDeveloper = "INSERT INTO APP.developers(id, name, email, address, bio) VALUES (?,?,?,?,?)";

	public Developer getDeveloper(int id) {
		PreparedStatement devSearch = DBConnection.prepare(developerSearch);
		try {
			devSearch.setInt(1, id);
			ResultSet rs = devSearch.executeQuery();
			if (rs.next()) {
				String name = rs.getString(2);
				String email = rs.getString(3);
				String password = rs.getString(4);
				String address = rs.getString(5);
				String bio = rs.getString(6);
				return new Developer(id, name, email, password, address, bio);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public Developer lookupDeveloperByEmail(String email) {
		PreparedStatement devSearch = DBConnection.prepare(developerEmailSearch);
		try {
			System.out.println("Email " + email);
			devSearch.setString(1, email);
			ResultSet rs = devSearch.executeQuery();
			if (rs.next()) {
				int id = rs.getInt(1);
				String name = rs.getString(2);
				String password = rs.getString(4);
				String address = rs.getString(5);
				String bio = rs.getString(6);
				return new Developer(id, name, email, password, address, bio);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void createDeveloper(Developer developer) {
		PreparedStatement devAdd = DBConnection.prepare(addDeveloper);
		try {
			devAdd.setInt(1, developer.getId());
			devAdd.setString(2, developer.getName());
			devAdd.setString(3, developer.getEmail());
			devAdd.setString(4, developer.getPassword());
			devAdd.setString(5, developer.getAddress());
			devAdd.setString(6, developer.getBio());
			devAdd.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
