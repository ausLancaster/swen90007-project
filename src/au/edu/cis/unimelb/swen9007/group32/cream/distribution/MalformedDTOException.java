package au.edu.cis.unimelb.swen9007.group32.cream.distribution;

public class MalformedDTOException extends Exception {
	public MalformedDTOException() {
		super("Malformed DTO");
	}
}
