package au.edu.cis.unimelb.swen9007.group32.cream.session;

import org.apache.shiro.SecurityUtils;

import au.edu.cis.unimelb.swen9007.group32.cream.domain.User;

public class AppSession {
	public static final String USER_ATTRIBUTE = "user";
	public static final String CUSTOMER_ROLE = "customer";
	public static final String DEVELOPER_ROLE = "developer";
	
	public static boolean hasRole(String role) {
		return SecurityUtils.getSubject().isAuthenticated();
	}
	
	public static boolean isAuthenticated() {
		return SecurityUtils.getSubject().isAuthenticated();
	}
	
	public static void init(User user) {
		SecurityUtils.getSubject().getSession().setAttribute(USER_ATTRIBUTE, user);
	}
	
	public static User getUser() {
		return (User) SecurityUtils.getSubject().getSession().getAttribute(USER_ATTRIBUTE);
	}
}
