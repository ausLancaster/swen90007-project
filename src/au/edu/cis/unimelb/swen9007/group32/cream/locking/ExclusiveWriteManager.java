package au.edu.cis.unimelb.swen9007.group32.cream.locking;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import au.edu.cis.unimelb.swen9007.group32.cream.data_access.DBConnection;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.Game;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.User;

public class ExclusiveWriteManager {

	private static final String LOCK_INSERT = "INSERT INTO APP.gameLock (gameID, owner) VALUES (?, ?)";
	private static final String LOCK_REMOVE = "DELETE FROM APP.gameLock WHERE gameID = ? and owner like ?";
	private static final String HAS_LOCK = "SELECT owner FROM APP.gameLock WHERE gameID = ?";

	private static ExclusiveWriteManager writeManager = null;
	
	public static ExclusiveWriteManager getInstance() {
		if (writeManager == null) {
			writeManager = new ExclusiveWriteManager();
		}
		return writeManager;
	}
	
	public boolean acquireLock(Game game, User user) {
		boolean result = true;
		if (!hasLock(game, user)) {
			try {
				PreparedStatement sqlPrepared = DBConnection.prepare(LOCK_INSERT);
				sqlPrepared.setInt(1, game.getId());
				sqlPrepared.setString(2, user.getEmail());
				sqlPrepared.executeUpdate();
			} catch (SQLException e) {
				result = false;
			}
		}
		return result;
	}
	
	public boolean hasLock(Game game, User user) {
		try {
			PreparedStatement sqlPrepared = DBConnection.prepare(HAS_LOCK);
			sqlPrepared.setInt(1, game.getId());
			System.out.println(sqlPrepared.toString());
			ResultSet rs = sqlPrepared.executeQuery();
			if (rs.next()) {
				String userEmail = rs.getString(1);
				if (userEmail.equals(user.getEmail())) {
					return true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}
	
	public void releaseLock(Game game, User user) {
		System.out.println("Releasing Lock");
		PreparedStatement sqlPrepared = DBConnection.prepare(LOCK_REMOVE);
		try {
			sqlPrepared.setInt(1, game.getId());
			sqlPrepared.setString(2, user.getEmail());
			sqlPrepared.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
