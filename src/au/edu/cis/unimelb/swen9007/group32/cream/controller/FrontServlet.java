package au.edu.cis.unimelb.swen9007.group32.cream.controller;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.util.Factory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.config.IniSecurityManagerFactory;

/**
 * Servlet implementation class FrontServlet
 */

public class FrontServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FrontServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void init(ServletConfig config) throws ServletException {
		System.out.println("init servlet");
		super.init(config);
		Factory<SecurityManager> factory = new IniSecurityManagerFactory("classpath:shiro.ini");
		SecurityManager securityManager = factory.getInstance();
		SecurityUtils.setSecurityManager(securityManager);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println(request.getServletPath());
		// Retrofit in Part A.
		if (request.getServletPath().equals("/index.html") || request.getServletPath().equals("/")
				|| request.getServletPath().equals("/search")) {
			getHomepage(request, response).process();
		} else if (request.getServletPath().equals("/purchases")) {
			getPurchases(request, response).process();
		} else if (request.getServletPath().equals("/logout")) {
			Subject currentUser = SecurityUtils.getSubject();
			currentUser.logout();
			getHomepage(request, response).process();
		} else if (request.getParameter("command") == null) {
			// Get the page by URL Path
			FrontCommand cmd = getUrl(request);
			if (cmd != null) {
				cmd.init(getServletContext(), request, response);
				cmd.process();
			}
		}
		response.getWriter().append("404 File Not Found");
	}

	private FrontCommand getUrl(HttpServletRequest request) {
		try {
			return (FrontCommand) getUrlClass(request).newInstance();
		} catch (Exception e) {
			return null;
		}
	}

	private Class getUrlClass(HttpServletRequest request) {
		Class result;
		final String commandClassName = "au.edu.cis.unimelb.swen9007.group32.cream.controller."
				+ (String) request.getServletPath().substring(1) + "Command";
		System.out.println(commandClassName);
		try {
			result = Class.forName(commandClassName);
			System.out.println(result);
		} catch (ClassNotFoundException e) {
			result = null;
		}
		return result;
	}

	private HomeCommand getHomepage(HttpServletRequest request, HttpServletResponse response) {
		HomeCommand hc = new HomeCommand();
		hc.init(getServletContext(), request, response);
		return hc;
	}

	private PurchasesCommand getPurchases(HttpServletRequest request, HttpServletResponse response) {
		PurchasesCommand pc = new PurchasesCommand();
		pc.init(getServletContext(), request, response);
		return pc;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
