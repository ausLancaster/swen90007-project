package au.edu.cis.unimelb.swen9007.group32.cream.domain;

import java.util.ArrayList;

public class DeveloperHelper {
	Developer dev;
	
	public String getName() {
		return dev.getName();
	}
	
	public DeveloperHelper(Developer dev) {
		this.dev = dev;
	}
	
	public ArrayList<Game> getAllDevelopedGames() {
		return dev.getAllDevelopedGames();
	}
}
