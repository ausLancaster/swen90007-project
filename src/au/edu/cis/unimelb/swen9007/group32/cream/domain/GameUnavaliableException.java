package au.edu.cis.unimelb.swen9007.group32.cream.domain;

public class GameUnavaliableException extends Exception {
	public GameUnavaliableException() {
		super("This game is Unavaliable");
	}
}
