package au.edu.cis.unimelb.swen9007.group32.cream.data_access;

import au.edu.cis.unimelb.swen9007.group32.cream.domain.Downloadable;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.Media;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.PhysicalMedia;

public class MediaMapper {
	PhysicalMediaMapper pmm;
	DownloadableMediaMapper dmm;
	
	public MediaMapper() {
		pmm = new PhysicalMediaMapper();
		dmm = new DownloadableMediaMapper();
	}
	
	public Media getMedia(int id) {
		Downloadable dm = (Downloadable) dmm.load(id);
		if (dm != null) {
			return dm;
		}
		PhysicalMedia pm = (PhysicalMedia) pmm.load(id);
		if (pm != null) {
			return pm;
		}
		return null;
	}
	
	public void saveMedia(Media media) {
		if (media instanceof PhysicalMedia) {
			pmm.save(media);
		}
		if (media instanceof Downloadable) {
			dmm.save(media);
		}
	}
}
