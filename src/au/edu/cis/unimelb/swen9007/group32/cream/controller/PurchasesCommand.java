package au.edu.cis.unimelb.swen9007.group32.cream.controller;

import java.io.IOException;

import javax.servlet.ServletException;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import au.edu.cis.unimelb.swen9007.group32.cream.session.AppSession;

public class PurchasesCommand extends FrontCommand {

	@Override
	public void process() throws ServletException, IOException {
		Subject currentUser = SecurityUtils.getSubject();
		if (currentUser.isAuthenticated() && currentUser.hasRole(AppSession.CUSTOMER_ROLE)) {
			forward("/Purchases.jsp");
		} else {
			forward("/AccessDenied.jsp");
		}
	}

}
