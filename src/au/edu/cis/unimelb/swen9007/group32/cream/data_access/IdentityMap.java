package au.edu.cis.unimelb.swen9007.group32.cream.data_access;

import java.util.HashMap;
import java.util.Map;

public class IdentityMap<T> {
	private Map<Long, T> map = new HashMap<Long, T>();
	
	private static Map<Class, IdentityMap> singletons = new HashMap<Class, IdentityMap>();
	
	public static <T> IdentityMap<T> getInstance(T t) {
		IdentityMap<T> result = singletons.get(t.getClass());
		if (result == null) {
			result = new IdentityMap<T>();
			singletons.put(t.getClass(), result);
		}
		return result;
	}
	
	public void put(long id,T obj) {
		map.put(id, obj);
	}
	
	public T get(long id) {
		return map.get(id);
	}
}
