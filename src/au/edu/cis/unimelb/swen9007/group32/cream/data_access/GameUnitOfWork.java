package au.edu.cis.unimelb.swen9007.group32.cream.data_access;

import au.edu.cis.unimelb.swen9007.group32.cream.domain.Game;

import java.util.ArrayList;
import java.util.List;

public class GameUnitOfWork {
	private static ThreadLocal current = new ThreadLocal();
	private List<Game> newObjects = new ArrayList<Game>();
	private List<Game> dirtyObjects = new ArrayList<Game>();
	private List<Game> deletedObjects = new ArrayList<Game>();

	public static void newCurrent() {
		setCurrent(new GameUnitOfWork());
	}
	
	public static void setCurrent(GameUnitOfWork uow) {
		current.set(uow);
	}
	
	public static GameUnitOfWork getCurrent() {
		return (GameUnitOfWork) current.get();
	}
	
	private boolean checksOk(Game game) {
		if (dirtyObjects.contains(game)) {
			System.out.println("Object is Dirty");
			return false;
		}
		if (deletedObjects.contains(game)) {
			System.out.println("Object is Deleted");
			return false;
		}
		if (newObjects.contains(game)) {
			System.out.println("Object is new");
			return false;
		}
		return true;
	}

	public void registerNew(Game newGame) {
		if (checksOk(newGame)) {
			newObjects.add(newGame);
		}
	}

	public void registerDirty(Game newGame) {
		if (newGame == null) {
			System.out.println("Game is null");
		}
		if (!dirtyObjects.contains(newGame) && !newObjects.contains(newGame)) {
			dirtyObjects.add(newGame);
		}
	}
	
	public void commit(GameMapper gameMap) {
		for (Game game : newObjects) {
			// Insert Game
			gameMap.insertGame(game);
		}
		for (Game game : dirtyObjects) {
			gameMap.updateGame(game);
		}
		for (Game game : deletedObjects) {
			gameMap.removeGame(game);
		}
	}

	public void registerDeleted(Game deletedGame) {
		if (deletedGame == null) {
			System.out.println("Game is null");
		}
		if (newObjects.remove(deletedGame)) {
			return;
		}
		dirtyObjects.remove(deletedGame);
		if (!deletedObjects.contains(deletedGame)) {
			deletedObjects.add(deletedGame);
		}
	}

}
