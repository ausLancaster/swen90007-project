package au.edu.cis.unimelb.swen9007.group32.cream.controller;

import java.io.IOException;
import javax.servlet.ServletException;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import au.edu.cis.unimelb.swen9007.group32.cream.data_access.GameMapper;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.Customer;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.Game;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.GameUnavaliableException;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.Payment;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.QuantityUnavaliableException;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.ServiceLayer;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.User;
import au.edu.cis.unimelb.swen9007.group32.cream.locking.LockingGameMapper;
import au.edu.cis.unimelb.swen9007.group32.cream.locking.NoLockException;
import au.edu.cis.unimelb.swen9007.group32.cream.session.AppSession;

public class BuyCommand extends FrontCommand {

	@Override
	public void process() throws ServletException, IOException {
		Subject currentUser = SecurityUtils.getSubject();
		if (currentUser.isAuthenticated() && currentUser.hasRole(AppSession.CUSTOMER_ROLE)) {
			Customer customer = (Customer) currentUser.getPrincipal();

			LockingGameMapper lgm = new LockingGameMapper(customer);
			try {
				Game game = lgm.getGame(Integer.parseInt(request.getParameter("id")));
				if (game == null) {
					forward("/Home.jsp");
				} else {
					int qty = Integer.parseInt(request.getParameter("qty"));
					Payment pay = ServiceLayer.makePayment(qty * game.getPrice(), Payment.CREDIT_CARD);
					try {
						customer.performPurchase(game, pay, qty);
						request.setAttribute("clientName", customer.getName());
						request.setAttribute("gameName", game.getTitle());
						forward("/PurchaseSuccess.jsp");
					} catch (GameUnavaliableException e) {
						lgm.abandonTransaction(game, customer);
						request.setAttribute("quantFail", "Sorry, the game is not avaliable in the quantity specified");
						forward("/Order.jsp");
					} catch (QuantityUnavaliableException e) {
						lgm.abandonTransaction(game, customer);
						request.setAttribute("quantFail", "Sorry, the game is not avaliable in the quantity specified");
						forward("/Order.jsp");
					}
				}
			} catch (NoLockException e) {
				System.out.println("Lock Not Avaliable");
				request.setAttribute("lockFail", "Unable to obtain lock: Please retry the transaction");
				forward("/Order.jsp");
			}
		} else {
			forward("/AccessDenied.jsp");
		}
	}

}
