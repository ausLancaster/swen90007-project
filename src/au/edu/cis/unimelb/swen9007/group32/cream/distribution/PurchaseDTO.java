package au.edu.cis.unimelb.swen9007.group32.cream.distribution;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.InputStream;
import java.io.OutputStream;

import au.edu.cis.unimelb.swen9007.group32.cream.domain.Game;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.Payment;

public class PurchaseDTO {
	private int id;
	private PaymentDTO payment;
	private GameDTO game;
	private int qty;
	
	public PurchaseDTO() {}
	
	public PurchaseDTO(int id, PaymentDTO payment, GameDTO game, int qty) {
		this.id = id;
		this.payment = payment;
		this.game = game;
		this.qty = qty;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public PaymentDTO getPayment() {
		return payment;
	}
	public void setPayment(PaymentDTO payment) {
		this.payment = payment;
	}
	public GameDTO getGame() {
		return game;
	}
	public void setGame(GameDTO game) {
		this.game = game;
	}
	public int getQty() {
		return this.qty;
	}
	
	public static void toXML(PurchaseDTO purchaseDTO, OutputStream outputStream) {
		XMLEncoder encoder = new XMLEncoder(outputStream);
		encoder.writeObject(purchaseDTO);
		encoder.close();
	}
	
	public static PurchaseDTO fromXML(InputStream inputStream) {
		XMLDecoder decoder = new XMLDecoder(inputStream);
		PurchaseDTO purDTO = (PurchaseDTO) decoder.readObject();
		decoder.close();
		return purDTO;
	}

}
