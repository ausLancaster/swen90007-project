package au.edu.cis.unimelb.swen9007.group32.cream.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import au.edu.cis.unimelb.swen9007.group32.cream.domain.Developer;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.DeveloperHelper;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.Game;
import au.edu.cis.unimelb.swen9007.group32.cream.domain.ServiceLayer;
import au.edu.cis.unimelb.swen9007.group32.cream.locking.LockingGameMapper;
import au.edu.cis.unimelb.swen9007.group32.cream.locking.NoLockException;
import au.edu.cis.unimelb.swen9007.group32.cream.session.AppSession;

public class UpdateQtyCommand extends FrontCommand {

	@Override
	public void process() throws ServletException, IOException {
		Developer dev;
		Subject currentUser = SecurityUtils.getSubject();
		if (currentUser.isAuthenticated() && currentUser.hasRole(AppSession.DEVELOPER_ROLE)) {
			dev = (Developer) currentUser.getPrincipal();
			LockingGameMapper lmm = new LockingGameMapper(dev);

			for (Game g : dev.getAllDevelopedGames()) {
				String param = request.getParameter(Integer.toString(g.getId()));
				if (param != "") {
					try {
						int increase = Integer.parseInt(param);
						// Grab a lock
						Game lockedGame = lmm.getGame(g.getId());
						lockedGame.setQty(lockedGame.getQty() + increase);
						lmm.updateGame(lockedGame);
					} catch (NoLockException e) {
						System.out.println("Unable to get Lock");
						lmm.abandonTransaction(g, dev);
					}
				}
			}
			request.setAttribute("helper", new DeveloperHelper(dev));
			forward("/Developer.jsp");
		} else {
			forward("/AccessDenied.jsp");
		}
	}
}
